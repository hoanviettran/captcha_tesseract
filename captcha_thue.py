import cv2
import numpy as np
from flask import request
from flask_restful import Resource
import base64
from response import response_json
import os
import pytesseract
import logging

TESSERACT_CONFIG = os.environ['TESSERACT_CONFIG']
TAX_BINARY_THRESHOLH = int(os.environ['TAX_BINARY_THRESHOLH'])
TAX_EROSION_KERNER = int(os.environ['TAX_EROSION_KERNER'])

class CaptchaTax(Resource):

    def get(self):
        return {'success': True}

    def post(self):
        # get json object
        json_obj = request.json
        if json_obj is None:
            # print(request.files)
            # print(dict(request.form))
            img_data = request.files.get('image', None)
            if img_data is None:
                return {'status_code': 403,
                'message': 'no form data'}
            img_data = img_data.read()
        # get base64-encoded data, then decode it and save to tmp
        else:
            img_data_base_64 = json_obj.get('data', None)
            if img_data_base_64 is None:
                return {'status_code': 403}
            img_data = base64.b64decode(img_data_base_64)

        image_bgr = cv2.imdecode(np.frombuffer(img_data, np.uint8), -1)
        
         # Image pre-processing
        img_gray = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2GRAY)
        ret, thresh1 = cv2.threshold(img_gray, TAX_BINARY_THRESHOLH, 255, cv2.THRESH_BINARY_INV)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(TAX_EROSION_KERNER, TAX_EROSION_KERNER))
        opened = cv2.morphologyEx(thresh1, cv2.MORPH_OPEN, kernel)
        opened_inv = 255 - opened

        # Text recognition using tesseract
        captcha_text = pytesseract.image_to_string(opened_inv, config=TESSERACT_CONFIG)

        json_respone = response_json(200, 'Success', captcha_text)
        logging.info(json_respone['Data'])
        return json_respone


