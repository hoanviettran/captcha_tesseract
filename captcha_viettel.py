import cv2
import numpy as np
from flask import request
from flask_restful import Resource
import base64
import pytesseract
from response import response_json
import os
# from google.cloud import vision
# from google.cloud.vision import types
import logging


TESSERACT_CONFIG = os.environ['TESSERACT_CONFIG']
VIETTEL_BINARY_THRESHOLH = int(os.environ['TAX_BINARY_THRESHOLH'])
VIETTEL_EROSION_KERNER = int(os.environ['TAX_EROSION_KERNER'])

# gg_image_context = types.ImageContext(language_hints =["en"])
# gg_vision_client = vision.ImageAnnotatorClient()

class CaptchaViettel(Resource):

    def get(self):
        return {'success': True}

    def post(self):
        # get json object
        json_obj = request.json
        if json_obj is None:
            # print(request.files)
            # print(dict(request.form))
            img_data = request.files.get('image', None)
            if img_data is None:
                return response_json(403, 'no form data')
            img_data = img_data.read()
        # get base64-encoded data, then decode it and save to tmp
        else:
            img_data_base_64 = json_obj.get('data', None)
            if img_data_base_64 is None:
                return response_json(403, 'no form data')
            img_data = base64.b64decode(img_data_base_64)

        try:
            image_bgr = cv2.imdecode(np.frombuffer(img_data, np.uint8), -1)

            # use google vision
            # success, encoded_image = cv2.imencode('.jpg', image_bgr)
            # content = encoded_image.tobytes()

            # image_gg = types.Image(content=content)
            # response = gg_vision_client.document_text_detection(image=image_gg, image_context=gg_image_context)
            # texts = response.text_annotations
            # if(len(texts) > 0):
            #     captcha_text = texts[0].description.split('\n')[0]
            # else:
            #     captcha_text = ''

            # use tesseract
            img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            img_blur = cv2.blur(img_gray,(VIETTEL_EROSION_KERNER, VIETTEL_EROSION_KERNER))
            ret,thresh1 = cv2.threshold(img_blur, VIETTEL_BINARY_THRESHOLH, 255, cv2.THRESH_BINARY)
            captcha_text = pytesseract.image_to_string(thresh1, config=TESSERACT_CONFIG)

            # Reponse
            json_respone = response_json(200, 'Success', captcha_text)
            logging.info(json_respone['Data'])
            return json_respone

        except Exception as e:
            logging.error(e)
            return response_json(500, 'Fail') 