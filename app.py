import os
from dotenv import load_dotenv
env_path = os.path.join(os.getcwd(), '.env')
load_dotenv(dotenv_path=env_path)

from flask import Flask, request
from flask_restful import Resource, Api
from captcha_bhxh import CaptchaBHXH
from captcha_thue import CaptchaTax
from captcha_viettel import  CaptchaViettel

import logging
logging.basicConfig(
    format='[%(asctime)s] [%(levelname)s] %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S %z')

app = Flask(__name__)
api = Api(app)

api.add_resource(CaptchaBHXH, '/captcha/bhxh')
api.add_resource(CaptchaViettel, '/captcha/viettel')
api.add_resource(CaptchaTax, '/captcha/thue')

if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', threaded=True)
